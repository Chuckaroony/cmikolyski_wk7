package ServletTest;

public class MadlibBuilder {


    public String MadlibBuilder(String[] nouns, String[] holidays, String[] persons,
                                String[] colors, String[] vegetables, String[] exclamations, String[] verbs) {
        String madlib = new StringBuilder()
                .append("<head><title>Madlib</title>").append("</head> <body> <h1> Halloween Treats Madlib! </h1> ")
                .append("<p>Tonights the "+nouns[0]+"! The one "+nouns[1]+" we all wait for, well next to ")
                .append(""+holidays[0]+" , "+persons[0]+" and I are going to be a "+nouns[2]+" and "+nouns[3]+". The "+nouns[4]+" turns ")
                .append(""+colors[0]+". The "+vegetables[0]+" are lit. We're ready to collect our "+nouns[5]+"! ")
                .append("A quick "+verbs[0]+" on the door to "+verbs[1]+" what we get. "+exclamations[0]+" it's a ")
                .append(""+vegetables[1]+", my favorite I bet! ")
                .append("More "+nouns[6]+" to "+verbs[2]+" upon, more treats to "+verbs[3]+", "+persons[1]+" and I ")
                .append("are finally done! ")
                .append(""+exclamations[1]+" for Halloween my favorite "+holidays[1]+"!</p> ")
                .append("</body>").toString();
        return madlib;
    }
}


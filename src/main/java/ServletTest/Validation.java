package ServletTest;
import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    public static boolean validate(String[] arrayToVal) {
        Pattern pattern = Pattern.compile("[^a-z0-9]",Pattern.CASE_INSENSITIVE);
        for (int i = 0; i < arrayToVal.length; i++) {
            if (arrayToVal[i] != null) {
                for (int j = 0; j < arrayToVal.length; j++) {
                    Matcher matcher = pattern.matcher(arrayToVal[j]);
                    boolean matchFound = matcher.find();
                    if (matchFound || arrayToVal[j].length() == 0) {
                        return false;
                    }
                }
            }
            else {
                return false;
            }
        }
        return true;
    }

    public static boolean toIterate(ArrayList<String[]> theList) {
        for (int i = 0; i < theList.size(); i++) {
            if (!validate(theList.get(i))) {
                return false;
            }
        }
        return true;
    }
}
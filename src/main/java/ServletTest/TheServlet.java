package ServletTest;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

@WebServlet(name = "TheServlet", urlPatterns = {"/TheServlet"})
public class TheServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        MadlibBuilder Madlib = new MadlibBuilder();
        response.setContentType("text/html");
        try {
            String[] nouns = request.getParameterValues("noun");
            String[] holidays = request.getParameterValues("holiday");
            String[] persons = request.getParameterValues("person");
            String[] colors = request.getParameterValues("color");
            String[] vegetables = request.getParameterValues("vegetable");
            String[] exclamations = request.getParameterValues("exclamation");
            String[] verbs = request.getParameterValues("verb");
            ArrayList formToList = new ArrayList();
            formToList.add(nouns);
            formToList.add(holidays);
            formToList.add(persons);
            formToList.add(colors);
            formToList.add(vegetables);
            formToList.add(exclamations);
            formToList.add(verbs);
            if (Validation.toIterate(formToList)){
                out.println(Madlib.MadlibBuilder(nouns,holidays,persons,colors,vegetables,
                        exclamations,verbs));
                System.out.println("Form data is valid. Responded to post request with Madlib using provided form data.");
            }
            else {
                out.println("<html><head></head><body><p> Please fill out all the fields with only one word and no spaces." +
                        " Please don't use special characters.</body></html>");
                System.out.println("Form data was invalid. Unable to respond to request with Madlib.");
            }

        } catch (Exception e){
            log("Unable to contact client.");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("The server is not accepting Get requests, sorry.");


    }
}

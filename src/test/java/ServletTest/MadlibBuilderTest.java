package ServletTest;

import org.junit.jupiter.api.Test;

class MadlibBuilderTest {
    MadlibBuilder madlibTest = new MadlibBuilder();
    @Test
    void madlibBuilderTest() {
        String[] nouns = {"Frog", "Dog", "Cat"};
        String[] holidays = {"Halloween", "Christmas"};
        String[] persons = {"Charly", "Dad", "Hannah"};
        String[] colors = {"red", "green", "yellow", "black"};
        String[] vegetables = {"pumpkin"};
        String[] exclamations ={"Hazah!"};
        String[] verbs = {"Run", "Hide" , "Sit"};

        String testhtml = madlibTest.MadlibBuilder(nouns, holidays, persons,colors,vegetables,exclamations, verbs);
        System.out.println(testhtml);

    }

}
